from setuptools import setup

#
###
setup(
  name="tcpserver",
  version="1.0.0",
  description="Python3 module to open tcp-socket server for remote requests",
  keywords="tcp socket remote requests",
  # Project's main page 
  #url="https://gitlab.com/mvimplis2013/tcp-client-server",
  url="https://gitlab.com/robert.berger/tcp-client-server",
  # Author details
  author="Miltos K. Vimplis", 
  author_email="mvimblis@gmail.com",
  # Choose your license
  license="MIT",
  classifiers=[
    "Programing Language :: Python :: 3.5",
    "Operating System :: Linux", 
    "Environment :: Console"
  ],
  packages=["tcpserver"],
  install_requires=["configparser"], 
#  entry_points={
#    "console_scripts": [
#      "tcpserver=tcpserver.server:main",
#     ],
#  },
  zip_safe=True
)
