#
#### Echo client: Attach the socket directly into a remote address
#
import socket 
import sys
import time 
import pickle

from . import *

CHUNK_SIZE = 30
RESPONSE_TERMINATION_CHARS = "@endofresponse@@" 

#
#### Connect to remote server - must be already running and waiting for requests.
#
def connect_remote( server_ip, open_port ):
    # Create a TCP/ip socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Connect socket to the remote port where server is listening
    server_address = ( server_ip, open_port )
    sock.connect( server_address )

    return sock

#
#### After connection established ... Send request to server and read response ... 
#
def send_receive( sock, message):
    try:
        sock.sendall( message.encode() )

        # Look for the response
        amount_received = 0
        data = ""
        
        while True:
                # Part of resposnse
                part = sock.recv( CHUNK_SIZE )
                #data = pickle.loads(data)
                #logger.debug("^^^ %s", part)
            
                amount_received += len(part)
                #logger.debug("Amount Received is ...%d/%d", amount_received, CHUNK_SIZE )
                
                # Byte array to string ... UTF-8
                data += part.decode()
                #data += part
                
                # Check response after last append ... do we have END-RESPONSE message ?
                if data.endswith( RESPONSE_TERMINATION_CHARS ):
                        # Response receive is ... Completed
                        break

        # Completed response 
        #logger.debug( "Received from server ... %s", data)

        return data

    finally:
        logger.debug("Closing socket !")
        sock.close()

#def main():
def send_request(host, port, message = "Dummy Message from TCP-Client to Server" ):
    #logger.debug( "Ready to Send Request to TCP-Server ... %s", message )

    sock = connect_remote( host, port ) # 
    
    response = send_receive( sock, message )

    return response

#if __name__ == "__main__":
#    main()