#
#### Echo server
#
import socket 
import sys
import pickle

from . import *

CHUNK_SIZE_BYTES = 30
RESPONSE_TERMINATION_CHARS = "@endofresponse@@" 

SEPARATOR = ",:"

#
#### Associate the server with a local address 
def bind_local_address( host, port ):
    # Create a TCP/IP socket
    sock = socket.socket( socket.AF_INET, socket.SOCK_STREAM )

    # Bind the socket to the port
    server_address = (host, port)
    sock.bind( server_address )

    return sock

#
#### Mark socket as accepting connections
def accept_connections( sock):
    # Calling listen() puts the socket into server mode 
    logger.debug("Waiting for Remote Requests ... Listening on %s", sock.getsockname())
    # Queue up as many as 5 connect requests
    sock.listen(5)

    while True:
        # Wait for an incoming connection ... with accept()
        # Accept() returns 1) Open connection at server, and 2) Address of client
        (connection, client_address) = sock.accept()

        try:
            logger.debug( "Connection from ... %s", client_address )

            # Receive the data in small chunks and retransmit it
            while True:
                # Data is read from the connection with recv() and transmitted with sendall()
                req = connection.recv( CHUNK_SIZE_BYTES )
                #logger.debug("Received '%s'", req)

                if req: 
                    requtf8 = req.decode("utf-8")
                    #logger.debug("Request UTF-8 ... %s", requtf8)

                    from nmapwrapper.network.nmap_wrapper import NMapWrapper
                    nmapwrapper = NMapWrapper(requtf8)
                    targets = nmapwrapper.handle_new_request()
                        
                    logger.debug("Sending NMAP response back to client")
                    
                    # Pass Array/ List over socket
                    #data = pickle.dumps(targets)
                    #connection.sendall( data )

                    # From list to string 
                    jointargets = SEPARATOR.join( targets  )
                    # Append termination text
                    jointargets += RESPONSE_TERMINATION_CHARS
                    
                    # UTF-8 encoding
                    connection.sendall( jointargets.encode() )

                    #connection.sendall( targets.encode() )
                else:
                    logger.debug("No more data from '%s'", client_address)
                    break
        finally:
            # Clean up the connection
            connection.close()

# 
#### Application Entrypoint
def prepare_agent_for_remote_requests( component="NMAP", host = "0.0.0.0", port = 5000, on_actions = None ):
    logger.debug(
        "Ready to Start Agent ... Responsible for Listening Remote Requests for %s", component)
    
    sock = bind_local_address( host, port )
    
    accept_connections(sock)
#### End of Application Entrypoint
    